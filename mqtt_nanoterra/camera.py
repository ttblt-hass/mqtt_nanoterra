"""Camera mode module."""
import json
from http import server
import io
import logging
import os
import signal
import socketserver
from threading import Condition
import queue

import picamera


SUPPORTED_RESOLUTIONS = ('640x480', '1280x720', '1920x1080')
PAGE = """\
<html>
<head>
<title>picamera MJPEG streaming demo</title>
</head>
<body>
<h1>PiCamera MJPEG Streaming Demo</h1>
<h2>Resolution</h2>
<a href="/640x480">640x480</a>
<a href="/1280x720">1280x720</a>
<a href="/1920x1080">1920x1080</a>
<h2>Stream</h2>
<img src="stream.mjpg" width="{}" height="{}" />
</body>
</html>
"""


class StreamingOutput():
    """Helper class to stream camera."""

    def __init__(self):
        """Class constructor."""
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        """Write camera data to a buffer."""
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)


class StreamingHandler(server.BaseHTTPRequestHandler):
    """HTTP request handler."""

    def do_POST(self):  # pylint: disable=invalid-name
        """Post requests handler."""
        if self.path == '/resolution':
            return_code = None
            if 'Content-Length' not in self.headers:
                return_code = 500
                content = {'error': 'Bad post content'}
            else:
                content_length = int(self.headers['Content-Length'])
                post_data = self.rfile.read(content_length)
                data = None
                try:
                    data = json.loads(post_data)['resolution']
                except json.decoder.JSONDecodeError:
                    return_code = 500
                    content = {'error': 'Post content not in json format'}
                except KeyError:
                    return_code = 500
                    content = {'error': 'Missing key "resolution" in the posted json'}
                if return_code != 500 and data not in SUPPORTED_RESOLUTIONS:
                    return_code = 500
                    content = {'error': 'Resolution shoud be one '
                                        'of {}'.format(",".join(SUPPORTED_RESOLUTIONS))}
                elif data in SUPPORTED_RESOLUTIONS:
                    self.server.cam_queue.put(data)
                    return_code = 200
                    content = {'resolution': self.server.resolution}
            self.send_response(return_code)
            self.send_header('Content-Type', 'application/json')
            content = json.dumps(content).encode('utf-8')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
            if return_code == 200:
                self.server.stop()
    def do_GET(self):  # pylint: disable=invalid-name
        """Get requests handler."""
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = PAGE.format(*self.server.resolution.split("x")).encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with self.server._output.condition:
                        self.server._output.condition.wait()
                        frame = self.server._output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as exp:  # pylint: disable=broad-except
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(exp))
        elif self.path == '/resolution':
            content = json.dumps({'resolution': self.server.resolution}).encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path in ('/640x480', '/1280x720', '/1920x1080'):
            self.server.cam_queue.put(self.path[1:])
            self.send_response(302)
            self.send_header('Location', "/index.html")
            self.end_headers()
            self.server.stop()
        else:
            self.send_error(404)
            self.end_headers()


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    """HTTP streaming server."""

    allow_reuse_address = True
    daemon_threads = True

    def __init__(self, output, resolution, cam_queue, server_address, logger, RequestHandlerClass):
        """Class constructor."""
        server.HTTPServer.__init__(self, server_address, RequestHandlerClass)
        self.logger = logger.getChild("http")
        self._output = output
        self.cam_queue = cam_queue
        self._resolution = resolution
        self.stopped = False

    @property
    def resolution(self):
        """Get current resolution."""
        return self._resolution

    def serve_forever(self, poll_interval=0.5):
        """Serve HTTP requests."""
        while not self.stopped:
            self.handle_request()

    def stop(self):
        """Stop HTTP server."""
        self.logger.info("Stopping http server")
        self.server_close()
        self.stopped = True


class Camera:
    """RPi Camera."""

    def __init__(self):
        """Class constructor."""
        self._get_logger()
        self.read_config()
        self._address = ('', self.port)
        self.output = StreamingOutput()
        self.queue = queue.Queue()
        self.restart = True
        signal.signal(signal.SIGINT, self._signal_handler)

    def read_config(self):
        """Read config from env vars."""
        self.resolution = os.environ.get("CAM_RESOLUTION", "640x480")
        self.framerate = int(os.environ.get("CAM_FRAMERATE", 24))
        self.port = int(os.environ.get("CAM_PORT", 8000))

    def _signal_handler(self, signal_, frame):
        """Signal handler."""
        self.logger.info("SIGINT received")
        self.logger.debug("Signal %s in frame %s received", signal_, frame)
        self.server.stop()
        self.logger.info("Exiting...")

    def _get_logger(self):
        """Build logger."""
        self._loglevel = os.environ.get('LOG_LEVEL', 'DEBUG').upper()
        self.logger = logging.getLogger(name="nanoterra").getChild("cam")
        self.logger.setLevel(getattr(logging, self._loglevel))
        console_handler = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
        console_handler.setFormatter(formatter)
        self.logger.addHandler(console_handler)

    def run(self):
        """Run daemon."""
        while self.restart:
            self.server = StreamingServer(self.output, self.resolution, self.queue,
                                          self._address, self.logger, StreamingHandler)
            self.restart = False
            with picamera.PiCamera(resolution=self.resolution, framerate=self.framerate) as camera:
                self.logger.info("Starting camera")
                camera.start_recording(self.output, format='mjpeg')
                try:
                    self.logger.info("Starting HTTP server")
                    self.server.serve_forever()
                finally:
                    camera.stop_recording()
                try:
                    self.resolution = self.queue.get(block=False)
                    self.logger.info("Changing resolution to %s", self.resolution)
                    self.restart = True
                except queue.Empty:
                    self.restart = False
