"""Module defining entrypoint."""
import asyncio

from mqtt_nanoterra import device
from mqtt_nanoterra import camera


def nanoterra_mqtt():
    """Entrypoint for MQTT sensor and light mode."""
    dev = device.MqttNanoTerra()
    asyncio.run(dev.async_run())


def nanoterra_cam():
    """Entrypoint for camera mode."""
    cam = camera.Camera()
    cam.run()
