"""MQTT mode module."""
import asyncio
import json
import os
import socket
import time
import uuid

from gpiozero import PWMLED
import mqtt_hass_base
import smbus

from mqtt_nanoterra.__version__ import VERSION


MAIN_LOOP_WAIT_TIME = 10

DEVICE_CLASSES = (None,  # Generic sensor. This is the default and doesn’t need to be set.
                  "battery",  # Percentage of battery that is left.
                  "humidity",  # Percentage of humidity in the air.
                  "illuminance",  # The current light level in lx or lm.
                  "signal_strength",  # Signal strength in dB or dBm.
                  "temperature",  # Temperature in °C or °F.
                  "power",  # Power in W or kW.
                  "pressure",  # Pressure in hPa or mbar.
                  "timestamp",  # Datetime object or timestamp string.
                  )


def get_mac():
    """Get mac address."""
    mac_addr = (':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff)
                for ele in range(0, 8 * 6, 8)][::-1]))
    return mac_addr


# https://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/
def get_raspi_model():
    """Get Raspberry Pi model."""
    model = None
    if os.path.isfile('/proc/device-tree/model'):
        with open("/proc/device-tree/model", 'rb') as fhm:
            model = fhm.read()
    model = model.strip(b'\x00').decode('utf-8')
    return model


def get_raspi_serial():
    """Get Raspberry Pi serial."""
    serial = None
    if os.path.isfile('/proc/device-tree/serial-number'):
        with open("/proc/device-tree/serial-number", 'rb') as fhs:
            serial = fhs.read()
    serial = serial.strip(b'\x00').decode('utf-8')
    return serial


def get_hostname():
    """Get Hostname."""
    return socket.gethostname()


class MqttNanoTerra(mqtt_hass_base.MqttDevice):
    """MQTT NanoTerra."""

    def __init__(self):
        """Class Constructor."""
        # DHT12
        self.i2c_bus = None
        self._dht12_addr = 0x5C
        self._humidity = None
        self._temperature = None
        # LED
        self.led = None
        self._led_brightless = 0
        # MQTT
        mqtt_hass_base.MqttDevice.__init__(self, "mqtt-nanoterra")
        self._topic_temperature_config = "/".join((self.mqtt_root_topic, "sensor",
                                                   self.name + "_temperature", "config"))
        self._topic_temperature_state = "/".join((self.mqtt_root_topic, "sensor",
                                                  self.name + "_temperature", "state"))

        self._topic_humidity_config = "/".join((self.mqtt_root_topic, "sensor",
                                                self.name + "_humidity", "config"))
        self._topic_humidity_state = "/".join((self.mqtt_root_topic, "sensor",
                                               self.name + "_humidity", "state"))

        self._topic_light_config = "/".join((self.mqtt_root_topic, "light",
                                            self.name, "light", "config"))
        self._topic_light_state = "/".join((self.mqtt_root_topic, "light",
                                            self.name, "state"))
        self._topic_light_brightness = "/".join((self.mqtt_root_topic, "light",
                                                 self.name, "brightness"))
        self._topic_light_command = "/".join((self.mqtt_root_topic, "light",
                                              self.name, "command"))
        self._topic_light_setbrightness = "/".join((self.mqtt_root_topic, "light",
                                                    self.name, "set_brightness"))

    def read_config(self):
        """Read env vars."""
        # Get led pin id
        if 'LED_PIN' in os.environ:
            self.led = PWMLED(os.environ['LED_PIN'],
                              frequency=int(os.environ.get('LED_FREQUENCY', 500)),
                              active_high=os.environ.get('LED_ACTIVE_HIGH', False))
        else:
            self.logger.warning('LED_PIN env var is missing, led is disabled')
        # Get dht12 i2c addr
        if 'DHT12_ADDR' in os.environ:
            self._dht12_addr = int(os.environ['DHT12_ADDR'], 0)
        # 1 should be a parameter
        self.i2c_bus = smbus.SMBus(1)  # pylint: disable=c-extension-no-member
        wait_time = 0.1
        timeout = 0
        while self.i2c_bus is None and timeout < 5:
            timeout += wait_time
            time.sleep(wait_time)

        if self.i2c_bus is None:
            self.logger.warning("Temperature sensor disabled")
        else:
            self.logger.debug("Temperature sensor activated")
            # Check if the dht12 is there
            self._read_temp_hum_raw()
            # small sleep to avoid to REquery to quickly the DHT12
            time.sleep(.1)
        # Get Offsets
        self._temperature_offset = float(os.environ.get("TEMPERATURE_OFFSET", 0))
        self._humidity_offset = float(os.environ.get("HUMIDITY_OFFSET", 0))

    @property
    def humidity(self):
        """Get humidity collected."""
        return self._humidity + self._humidity_offset

    @property
    def temperature(self):
        """Get temperature collected."""
        return self._temperature + self._temperature_offset

    def _read_temp_hum_raw(self):
        """Read data from DHT12 sensor."""
        # Read 5 bytes of data from the device address (0x05C) starting from an offset of zero
        try:
            data = self.i2c_bus.read_i2c_block_data(self._dht12_addr, 0x00, 5)
        except OSError:
            self.logger.warning('DHT12 sensor not found at address %s', hex(self._dht12_addr))
            self.i2c_bus = None
            self._humidity = None
            self._temperature = None
            return
        except AttributeError:
            self.logger.warning('DHT12 sensor not found at address %s', hex(self._dht12_addr))
            self.i2c_bus = None
            self._humidity = None
            self._temperature = None
            return
        # Check data
        if data[0] + data[1] + data[2] + data[3] == data[4]:
            self.logger.debug("DHT12 data checksum is correct")
        else:
            self.logger.error("DHT12 data checksum is incorrect")
            self._humidity = None
            self._temperature = None
            return
        self._humidity = float("{}.{}".format(data[0], data[1]))
        self._temperature = float("{}.{}".format(data[2], data[3]))

    async def _init_main_loop(self):
        """Init before starting main loop."""

    async def _main_loop(self):
        """Run main loop."""
        # Get data
        self._read_temp_hum_raw()

        # Send Temperature
        if self.i2c_bus and self.temperature:
            self.mqtt_client.publish(topic=self._topic_temperature_state,
                                     payload="{:0.2f}".format(self.temperature))
        # Send Humidity
        if self.i2c_bus and self.humidity:
            self.mqtt_client.publish(topic=self._topic_humidity_state,
                                     payload="{:0.2f}".format(self.humidity))
        # Send LED data
        if self.led is not None:
            self.mqtt_client.publish(topic=self._topic_light_state,
                                     payload="OFF" if self.led.value == 0 else "ON")
            self.mqtt_client.publish(topic=self._topic_light_brightness,
                                     payload=self._led_brightless * 100)

        self.logger.debug("Data send")
        i = 0
        while i < MAIN_LOOP_WAIT_TIME and self.must_run:
            await asyncio.sleep(1)
            i += 1

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""
        config = {}
        mac_addr = get_mac()
        hostname = get_hostname()
        config["device"] = {"connections": [["mac", mac_addr]],
                            "name": hostname,
                            "via_device": hostname,
                            "sw_version": VERSION}
        model = get_raspi_model()
        if model is not None:
            config["device"]["model"] = model
        serial = get_raspi_serial()
        if serial is not None:
            config["device"]["identifiers"] = serial

        if self.i2c_bus is not None:
            # Temperature C
            config_c = config.copy()
            config_c.update({"state_topic": self._topic_temperature_state,
                             "name": self.name + " temperature",
                             "unique_id": self.name + "_temperature",
                             "unit_of_measurement": "C",
                             "force_update": True,
                             "device_class": "temperature",
                             # This seems create a new entities after the expire time
                             # "expire_after": MAIN_LOOP_WAIT_TIME * 3,
                             "qos": 0,
                             })
            self.logger.debug("%s: %s", self._topic_temperature_config, json.dumps(config_c))
            self.mqtt_client.publish(topic=self._topic_temperature_config,
                                     retain=True,
                                     payload=json.dumps(config_c))

            # Humidity
            config_c = config.copy()
            config_c.update({"state_topic": self._topic_humidity_state,
                             "name": self.name + " humidity",
                             "unique_id": self.name + "_humidity",
                             "unit_of_measurement": "%",
                             "force_update": True,
                             "device_class": "humidity",
                             # This seems create a new entities after the expire time
                             # "expire_after": MAIN_LOOP_WAIT_TIME * 3,
                             "qos": 0,
                             })
            self.logger.debug("%s: %s", self._topic_humidity_config, json.dumps(config_c))
            self.mqtt_client.publish(topic=self._topic_humidity_config,
                                     retain=True,
                                     payload=json.dumps(config_c))

        # light
        if self.led is not None:
            config_c = config.copy()
            config_c.update({"name": self.name + " light",
                             "unique_id": self.name + "_light",
                             "command_topic": self._topic_light_command,
                             "brightness_command_topic": self._topic_light_setbrightness,
                             "brightness_scale": 100,
                             "brightness_state_topic": self._topic_light_brightness,
                             "state_topic": self._topic_light_state,
                             "payload_on": "ON",
                             "payload_off": "OFF",
                             "qos": 0,
                             "optimistic": False})
            self.logger.debug("New light config: %s, %s",
                              self._topic_light_config,
                              json.dumps(config_c))
            self.mqtt_client.publish(topic=self._topic_light_config,
                                     retain=True,
                                     payload=json.dumps(config_c))

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""
        if self.led is not None:
            self.mqtt_client.subscribe(self._topic_light_command)
            self.mqtt_client.subscribe(self._topic_light_setbrightness)

    def _on_message(self, client, userdata, msg):
        """MQTT on message callback."""
        self.logger.debug("Message: %s %s", msg.topic, str(msg.payload))
        try:
            if msg.topic == self._topic_light_command and msg.payload == b"ON":
                # Turn On
                if self._led_brightless:
                    self.led.value = self._led_brightless
                else:
                    self.led.value = self._led_brightless = 1
            elif msg.topic == self._topic_light_command and msg.payload == b"OFF":
                # Turn Off
                self.led.value = 0
            elif msg.topic == self._topic_light_setbrightness:
                # Change brightness
                brightness = int(msg.payload)
                if brightness < 0 or brightness > 100:
                    self.logger.error('Bad brightness value: %s', brightness)
                self.led.value = self._led_brightless = brightness / 100
            else:
                self.logger.warning("Unknown topic: %s %s", msg.topic, str(msg.payload))
                return
        except BaseException as exp:
            self.logger.error(exp)
        # Send light states
        self.mqtt_client.publish(topic=self._topic_light_state,
                                 payload="OFF" if self.led.value == 0 else "ON")
        self.mqtt_client.publish(topic=self._topic_light_brightness,
                                 payload=self._led_brightless * 100)

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""
        # Delete Temp
        self.logger.debug("Deleting %s: %s", self._topic_temperature_config, json.dumps({}))
        self.mqtt_client.publish(topic=self._topic_temperature_config,
                                 retain=False,
                                 payload=json.dumps({}))
        # Delete humidity
        self.logger.debug("Deleting %s: %s", self._topic_humidity_config, json.dumps({}))
        self.mqtt_client.publish(topic=self._topic_humidity_config,
                                 retain=False,
                                 payload=json.dumps({}))
        # Delete light
        self.logger.info("Deleting %s: %s", self._topic_light_config, json.dumps({}))
        self.mqtt_client.publish(topic=self._topic_light_config,
                                 retain=False,
                                 payload=json.dumps({}))

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
